require.config({
    paths: {
        jquery: 'http://code.jquery.com/jquery-2.1.4.min',
        underscore: 'http://underscorejs.org/underscore-min',
        backbone: 'http://backbonejs.org/backbone-min',
        highcharts: 'http://code.highcharts.com/highcharts'
    }
});
require([
    'jquery',
    'backbone',
    'collection',
    'views/reports',
    'views/report'
], function($, Backbone, collection, Reports, Report) {

    var Router = Backbone.Router.extend({
        routes: {
            '': 'reports',
            'report/:id': 'report'
        },
        initialize: function(){},
        reports: function () {
            $('body').html(new Reports({collection: collection}).render().$el);
        },
        report: function (id) {
            $('body').html(new Report({model: collection.get(id)}).render().$el);
        }
    });
    var router = new Router();

    Backbone.history.start();
});