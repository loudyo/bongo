define(['backbone'], function(Backbone){
    return new Backbone.Collection([
        {
            id: 1,
            "name": "name 1",
            "description": "description 1",
            "type": "type1",
            "data": [1, 0, 4]
        },
        {
            id: 2,
            "name": "name 2",
            "description": "description 2",
            "type": "chart",
            "data": [5, 7, 3]
        }
    ]);
})