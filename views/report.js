define([
    'underscore',
    'jquery',
    'backbone',
    'highcharts'
], function(_, $, Backbone) {
    return Backbone.View.extend({
        render: function(){
            $(this.el).highcharts({
                series: [{
                    data: this.model.get('data')
                }]
            });
            return this
        }
    })
})