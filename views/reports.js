define([
    'underscore',
    'backbone',
    'text!templates/reports.html'
], function(_, Backbone, tmpl) {
    return Backbone.View.extend({
        template: _.template(tmpl),
        render: function(){
            this.$el.html(this.template({collection: this.collection}));
            return this;
        }
    })
})